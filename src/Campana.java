class Campana extends Thread {
    String suono;
    int volte;
    int ritardo;
    public Campana(String suono, int volte, int ritardo){
        this.suono=suono;
        this.volte=volte;
        this.ritardo=ritardo;
    }

    public void run(){
        for (int k=0; k<volte; k++){
            try {
                this.sleep(ritardo);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print((k+1)+suono+" ");
        }
    }
}